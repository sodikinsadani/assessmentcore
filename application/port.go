package application

import (
	"github.com/gofiber/fiber/v2"
	"github.com/jmoiron/sqlx"
	"gitlab.com/sodikinsadani/assessmentcore/data"
)

type AssessmentCoreApiPort interface {
	SetupRoutes(app *fiber.App)
	GetAllUsers(ctx *fiber.Ctx) error
	RegistrasiRekening(ctx *fiber.Ctx) error
	SetorTunai(ctx *fiber.Ctx) error
	TarikTunai(ctx *fiber.Ctx) error
	CekSaldo(ctx *fiber.Ctx) error
	InfoMutasi(ctx *fiber.Ctx) error
}

type AssessmentCoreAppPort interface {
	GetAllUsersApp(ctx *fiber.Ctx) error
	RegistrasiRekeningApp(ctx *fiber.Ctx) error
	SetorTunaiApp(ctx *fiber.Ctx) error
	TarikTunaiApp(ctx *fiber.Ctx) error
	CekSaldoApp(ctx *fiber.Ctx) error
	InfoMutasiApp(ctx *fiber.Ctx) error
}

type AssessmentCoreDatastorePort interface {
	Begin() (tx *sqlx.Tx, err error)
	Rollback(tx *sqlx.Tx)
	Commit(tx *sqlx.Tx)

	GetAllUsersDb(tx *sqlx.Tx) (users []data.Users, err error)
}
