package application

import "gitlab.com/sodikinsadani/assessmentcore/logging"

type AssessmentCoreApplication struct {
	datastore AssessmentCoreDatastorePort
	log       *logging.Logger
}

func InitApplication(datastore AssessmentCoreDatastorePort, log *logging.Logger) *AssessmentCoreApplication {
	return &AssessmentCoreApplication{
		datastore: datastore,
		log:       log,
	}
}
