package application

import (
	"fmt"

	"github.com/gofiber/fiber/v2"
	"github.com/sirupsen/logrus"
	"gitlab.com/sodikinsadani/assessmentcore/data"
)

func (c *AssessmentCoreApplication) GetAllUsersApp(ctx *fiber.Ctx) error {
	tx, err := c.datastore.Begin()
	if err != nil {
		err = fmt.Errorf("failed to begin transaction")
		c.log.Warn(logrus.Fields{}, nil, err.Error())
		c.datastore.Rollback(tx)
		return ctx.Status(400).JSON(fiber.Map{
			"status": "02",
			"remark": err,
		})
	}

	usersData, err := c.datastore.GetAllUsersDb(tx)
	if err != nil {
		err = fmt.Errorf("failed to get users data")
		c.log.Warn(logrus.Fields{}, nil, err.Error())
		c.datastore.Rollback(tx)
		return ctx.Status(400).JSON(fiber.Map{
			"status": "02",
			"remark": "failed to get users data",
		})
	}

	c.datastore.Commit(tx)

	return ctx.Status(200).JSON(fiber.Map{
		"status": "00",
		"remark": "List Data Users",
		"data":   usersData,
	})
}

func (c *AssessmentCoreApplication) RegistrasiRekeningApp(ctx *fiber.Ctx) error {
	var NasabahEksis = map[string]string{
		"nama":        "sodikin",
		"nik":         "3209784556344567",
		"no_hp":       "082156767887",
		"no_rekening": "002676867687",
	}

	rekening := new(data.RekeningRequest)
	if err := ctx.BodyParser(rekening); err != nil {
		return ctx.Status(400).JSON(fiber.Map{
			"status": "02",
			"remark": fmt.Sprintf("%s", err),
		})
	}
	if rekening.Nik == NasabahEksis["nik"] && rekening.NoHp == NasabahEksis["no_hp"] {
		return ctx.Status(400).JSON(fiber.Map{
			"status": "02",
			"remark": "nik dan dan no_hp sudah digunakan",
			"data":   NasabahEksis,
		})
	}

	rekening.NoRekening = fmt.Sprintf("%010d", 12) //hasil generate
	return ctx.Status(200).JSON(fiber.Map{
		"status": "00",
		"remark": "Registrasi Nasabah Berhasil",
		"data":   rekening,
	})
}

func (c *AssessmentCoreApplication) SetorTunaiApp(ctx *fiber.Ctx) error {
	var InfoRekening = map[string]interface{}{
		"nama":        "sodikin",
		"nik":         "3209784556344567",
		"no_hp":       "082156767887",
		"no_rekening": "002676867687",
		"saldo":       1000000,
	}

	st := new(data.SetorTunaiRequest)
	if err := ctx.BodyParser(st); err != nil {
		return ctx.Status(400).JSON(fiber.Map{
			"status": "02",
			"remark": fmt.Sprintf("%s", err),
		})
	}

	if st.NoRekening != InfoRekening["no_rekening"] {
		return ctx.Status(400).JSON(fiber.Map{
			"status": "02",
			"remark": "no_rekening tidak terdaftar di table rekening. Silahkan isi nomor_rekening dengan benar",
		})
	}

	// vSaldo, _ := strconv.ParseFloat(InfoRekening["saldo"], 64)
	return ctx.Status(200).JSON(fiber.Map{
		"status": "00",
		"remark": "Setor Tunai Berhasil",
		"saldo":  st.Nominal + 2000000,
	})
}

func (c *AssessmentCoreApplication) TarikTunaiApp(ctx *fiber.Ctx) error {
	var InfoRekening = map[string]interface{}{
		"nama":        "sodikin",
		"nik":         "3209784556344567",
		"no_hp":       "082156767887",
		"no_rekening": "002676867687",
		"saldo":       1000000,
	}

	tt := new(data.TarikTunaiRequest)
	if err := ctx.BodyParser(tt); err != nil {
		return ctx.Status(400).JSON(fiber.Map{
			"status": "02",
			"remark": fmt.Sprintf("%s", err),
		})
	}

	if tt.NoRekening != InfoRekening["no_rekening"] {
		return ctx.Status(400).JSON(fiber.Map{
			"status": "02",
			"remark": "no_rekening tidak terdaftar di table rekening. Silahkan isi nomor_rekening dengan benar",
		})
	}

	if tt.Nominal > 2000000 {
		return ctx.Status(400).JSON(fiber.Map{
			"status": "02",
			"remark": "saldo rekening tidak cukup. saldo saat ini 2000000",
		})
	}

	// vSaldo, _ := strconv.ParseFloat(InfoRekening["saldo"], 64)
	return ctx.Status(200).JSON(fiber.Map{
		"status": "00",
		"remark": "Tarik Tunai Berhasil",
		"saldo":  2000000 - tt.Nominal,
	})
}

func (c *AssessmentCoreApplication) CekSaldoApp(ctx *fiber.Ctx) error {
	var InfoRekening = map[string]interface{}{
		"nama":        "sodikin",
		"nik":         "3209784556344567",
		"no_hp":       "082156767887",
		"no_rekening": "002676867687",
		"saldo":       1000000,
	}

	if ctx.Params("no_rekening") != InfoRekening["no_rekening"] {
		return ctx.Status(400).JSON(fiber.Map{
			"status": "02",
			"remark": "no_rekening tidak terdaftar di table rekening. Silahkan isi nomor_rekening dengan benar",
		})
	}

	return ctx.Status(200).JSON(fiber.Map{
		"status": "00",
		"remark": fmt.Sprintf("Informasi saldo rekening %s", ctx.Params("no_rekening")),
		"saldo":  2000000,
	})
}

func (c *AssessmentCoreApplication) InfoMutasiApp(ctx *fiber.Ctx) error {
	var InfoRekening = map[string]interface{}{
		"nama":        "sodikin",
		"nik":         "3209784556344567",
		"no_hp":       "082156767887",
		"no_rekening": "002676867687",
		"saldo":       1000000,
	}

	if ctx.Params("no_rekening") != InfoRekening["no_rekening"] {
		return ctx.Status(400).JSON(fiber.Map{
			"status": "02",
			"remark": "no_rekening tidak terdaftar di table rekening. Silahkan isi nomor_rekening dengan benar",
		})
	}

	var mutasi = []map[string]interface{}{
		{"waktu": "2023-01-01", "kode_transaksi": "C", "nominal": 2000000},
		{"waktu": "2023-01-03", "kode_transaksi": "C", "nominal": 10000},
	}

	return ctx.Status(200).JSON(fiber.Map{
		"status": "00",
		"remark": fmt.Sprintf("Informasi saldo rekening %s", ctx.Params("no_rekening")),
		"mutasi": mutasi,
	})
}
