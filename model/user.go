package model

type User struct {
	// gorm.Model
	// ID       uuid.UUID `gorm:"type:uuid;"`
	Username string `json:"username"`
	Email    string `json:"email"`
	Password string `json:"password"`
}

type Users struct {
	Users []User `json:"users"`
}

// func (user *User) BeforeCreate(tx *gorm.DB) (err error) {

// 	user.ID = uuid.New()
// 	return
// }

// type Rekening struct {
// 	gorm.Model
// 	ID            uuid.UUID `gorm:"type:uuid;"`
// 	NomorRekening string    `json:"nomor_rekening"`
// 	NamaRekening  string    `json:"nama_rekening"`
// 	Nama          string    `json:"nama"`
// 	Nik           string    `json:"nik"`
// 	NoHp          string    `json:"no_hp"`
// }

// type Rekenings struct {
// 	Rekenings []Rekening `json:"rekenings"`
// }

// func (rekening *Rekening) BeforeCreate(tx *gorm.DB) (err error) {

// 	rekening.ID = uuid.New()
// 	return
// }
