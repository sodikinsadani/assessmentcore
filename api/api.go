package api

import (
	"fmt"

	"github.com/gofiber/fiber/v2"
	"gitlab.com/sodikinsadani/assessmentcore/application"
	"gitlab.com/sodikinsadani/assessmentcore/logging"
)

type AssessmentCoreAPI struct {
	app          application.AssessmentCoreAppPort
	api          application.AssessmentCoreApiPort
	log          *logging.Logger
	service_port string
}

func (a *AssessmentCoreAPI) Start() {
	app := fiber.New()
	app.Get("/", a.GetAllUsers)
	app.Post("/daftar", a.RegistrasiRekening)
	app.Post("/tabung", a.SetorTunai)
	app.Post("/tarik", a.TarikTunai)
	app.Get("/saldo/:no_rekening", a.CekSaldo)
	app.Get("/mutasi/:no_rekening", a.InfoMutasi)
	// app.Use(logger.New())
	// app.Use(cors.New())

	app.Use(func(c *fiber.Ctx) error {
		return c.SendStatus(404)
	})

	app.Listen(fmt.Sprintf(`:%s`, a.service_port))
}

func InitAssessmentCoreAPI(log *logging.Logger, app application.AssessmentCoreAppPort, port string) *AssessmentCoreAPI {
	return &AssessmentCoreAPI{
		log:          log,
		app:          app,
		service_port: port,
	}
}
