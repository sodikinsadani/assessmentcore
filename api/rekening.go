package api

import (
	"github.com/gofiber/fiber/v2"
)

func (c *AssessmentCoreAPI) GetAllUsers(ctx *fiber.Ctx) error {
	err := c.app.GetAllUsersApp(ctx)

	return err
}

func (c *AssessmentCoreAPI) RegistrasiRekening(ctx *fiber.Ctx) error {
	err := c.app.RegistrasiRekeningApp(ctx)

	return err
}

func (c *AssessmentCoreAPI) SetorTunai(ctx *fiber.Ctx) error {
	err := c.app.SetorTunaiApp(ctx)

	return err
}

func (c *AssessmentCoreAPI) TarikTunai(ctx *fiber.Ctx) error {
	err := c.app.SetorTunaiApp(ctx)

	return err
}

func (c *AssessmentCoreAPI) CekSaldo(ctx *fiber.Ctx) error {
	err := c.app.CekSaldoApp(ctx)

	return err
}

func (c *AssessmentCoreAPI) InfoMutasi(ctx *fiber.Ctx) error {
	err := c.app.InfoMutasiApp(ctx)

	return err
}
