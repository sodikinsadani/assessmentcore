package main

import (
	_ "github.com/godror/godror"
	_ "github.com/lib/pq"
	api "gitlab.com/sodikinsadani/assessmentcore/api"
	"gitlab.com/sodikinsadani/assessmentcore/application"
	database "gitlab.com/sodikinsadani/assessmentcore/datastore"
	"gitlab.com/sodikinsadani/assessmentcore/logging"

	_ "github.com/lib/pq"
	"github.com/spf13/viper"
)

func main() {
	viper.SetConfigName(".env")
	viper.SetConfigType("env")
	viper.AddConfigPath(".")
	viper.AutomaticEnv()
	err := viper.ReadInConfig()
	if err != nil {
		panic(err)
	}

	SERVICE := viper.GetString("SERVICE")
	SERVICE_PORT := viper.GetString("SERVICE_PORT")

	DB_DRIVER := viper.GetString("DB_DRIVER")
	DB_USER := viper.GetString("DB_USER")
	DB_PASSWORD := viper.GetString("DB_PASSWORD")
	DB_HOST := viper.GetString("DB_HOST")
	DB_PORT := viper.GetInt("DB_PORT")
	DB_DATABASE := viper.GetString("DB_DATABASE")

	DB_SCHEMAS := map[string]string{
		"public":      viper.GetString("PUBLIC_SCHEMA"),
		"core_schema": viper.GetString("CORE_SCHEMA"),
		"ent_schema":  viper.GetString("ENT_SCHEMA"),
	}

	logger := logging.NewLogger(SERVICE)
	ds := database.InitDatastore(DB_DRIVER, DB_HOST, DB_USER, DB_PASSWORD, DB_DATABASE, DB_PORT, DB_SCHEMAS, logger)
	application := application.InitApplication(ds, logger)
	api := api.InitAssessmentCoreAPI(logger, application, SERVICE_PORT)
	api.Start()
}
