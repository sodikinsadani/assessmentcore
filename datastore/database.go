package datastore

import (
	"fmt"

	"github.com/jmoiron/sqlx"
	"github.com/sirupsen/logrus"
	"gitlab.com/sodikinsadani/assessmentcore/logging"
)

type AssessmentCoreDatabase struct {
	db           *sqlx.DB
	log          *logging.Logger
	publicSchema string
	coreSchema   string
	entSchema    string
}

func (f *AssessmentCoreDatabase) Begin() (tx *sqlx.Tx, err error) {
	tx, err = f.db.Beginx()
	if err != nil {
		remark := "failed to start transaction"
		f.log.Error(logrus.Fields{
			"error": err.Error(),
		}, nil, remark)
		err = fmt.Errorf(remark)
	}
	return
}

func (f *AssessmentCoreDatabase) Rollback(tx *sqlx.Tx) {
	err := tx.Rollback()
	if err != nil {
		f.log.Error(logrus.Fields{
			"error": err.Error(),
		}, nil, "failed to rollback transaction")
	}
}

func (f *AssessmentCoreDatabase) Commit(tx *sqlx.Tx) {
	err := tx.Commit()
	if err != nil {
		f.log.Error(logrus.Fields{
			"error": err.Error(),
		}, nil, "failed to rollback transaction")
	}
}

func InitDatastore(driver, host, user, password, database string, port int, map_schema map[string]string, log *logging.Logger) *AssessmentCoreDatabase {
	var address string
	if driver == "postgres" {
		address = fmt.Sprintf("%s//%s:%s@%s:%d/%s", driver, user, password, host, port, database)
	} else if driver == "godror" {
		address = fmt.Sprintf(`user="%s" password="%s" connectString="(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST=%s)(PORT=%d))(CONNECT_DATA=(SID=%s)))"`, user, password, host, port, database)
	}
	db, err := sqlx.Connect(driver, address)
	if err != nil {
		panic(err)
	}
	return &AssessmentCoreDatabase{
		db:           db,
		log:          log,
		publicSchema: map_schema["public"],
		coreSchema:   map_schema["core_schema"],
		entSchema:    map_schema["ent_schema"],
	}
}
