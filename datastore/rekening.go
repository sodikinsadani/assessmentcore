package datastore

import (
	"fmt"

	"github.com/jmoiron/sqlx"
	"github.com/sirupsen/logrus"
	"gitlab.com/sodikinsadani/assessmentcore/data"
)

func (c *AssessmentCoreDatabase) GetAllUsersDb(tx *sqlx.Tx) (user []data.Users, err error) {
	sql := fmt.Sprintf(
		`select username, email
		from %s.users
		order by username
	`, c.entSchema)

	err = tx.Select(&user, sql)
	if err != nil {
		remark := "failed to get user data"

		c.log.Error(logrus.Fields{
			"error": err.Error(),
		}, nil, remark)
		err = fmt.Errorf(remark)
	}
	return
}
