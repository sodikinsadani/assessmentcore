package data

type RekeningRequest struct {
	Nama       string `json:"nama"`
	Nik        string `json:"nik"`
	NoHp       string `json:"no_hp"`
	NoRekening string `json:"no_rekening"`
}

type SetorTunaiRequest struct {
	NoRekening string  `json:"no_rekening"`
	Nominal    float32 `json:"nominal"`
	Saldo      float32 `json:"saldo"`
}

type TarikTunaiRequest struct {
	NoRekening string  `json:"no_rekening"`
	Nominal    float32 `json:"nominal"`
	Saldo      float32 `json:"saldo"`
}

type Rekening struct {
	Id            string `db:"id" json:"id"`
	NomorRekening string `db:"nomor_rekening" json:"nomor_rekening"`
	NamaRekening  string `db:"nama_rekening" json:"nama_rekening"`
	Nama          string `db:"nama" json:"nama"`
	Nik           string `db:"nik" json:"nik"`
	NoHp          string `db:"no_hp" json:"no_hp"`
}

type Users struct {
	Id        string `db:"ID" json:"id"`
	CreatedAt string `db:"CREATED_AT" json:"created_at"`
	UpdatedAt string `db:"UPDATED_AT" json:"updated_at"`
	DeletedAt string `db:"DELETED_AT" json:"deleted_at"`
	Username  string `db:"USERNAME" json:"username"`
	Password  string `db:"PASSWORD" json:"password"`
	Email     string `db:"EMAIL" json:"email"`
}
